# Charts

Available charts in this repo:
  * XNAT
  * TaskManager
  * StudyGovernor


## XNAT

### Available settings

  * deployment:
      * name: xnat
      * host: local-dev-xnat.infra-dev.k3d
      * account: rsvc-infra-dev
  * config:
    * global:
      * db_name: xnat
      * db_user: xnat
    * xnat:
      * data: /xnatdata/
      * home: /xnatdata/home
  * secrets:
    * global:
      * db_password: xnat-password

### Usage information and such
  Cehck the values file in ./xnat/values.
  Default admin password once the appliction is up:
  user:admin
  passwd:admin

## Task Manager
  
  * deployment:
    * name: sandbox
    * host: sandbox-taskmanager.infra-dev.k3d
  * config:
    * global:
      * db_name: taskmanager
      * db_user: taskmanager
    * taskmanager:
      * project_repository: git@example.  * com:user/project
  * secrets:
    * global:
      * db_password: "taskmanager_password"
    * mysql:
      * root_password: "mysql-root-password"
    * taskmanager:
      * secret_key: 390eyuigdjbnmcx3298yuiewgjsd
      * security_password_salt: 372yethwjdsr93th

  ### Usage information and such

Taskmanager should be running at the apropriate host. All other info like credentials should be known and in the project repository.
  
  ## StudyGovernor

  WIP